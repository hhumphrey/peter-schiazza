define(['utils/queue'], function(Queue) {	 
	var delayQueue = function(){
			this.queue=new Queue();			
			this.run = function(callback){
				  this.queue.enqueue(function(){
				   	callback.apply();
				  });
				  this.queue.flush();
				  return this;
			 };
			 this.delay = function(ms){
				 this.queue.enqueue('delay:' + ms);
				 return this;
			 };
			this.reset= function(){
				 this.queue.reset();
				 this.queue=new Queue();	
			 }
	}	
	return delayQueue;	
});
