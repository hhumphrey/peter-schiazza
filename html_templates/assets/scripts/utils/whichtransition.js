define([], function() {	
	function whichTransitionEvent(){
	    var t;
	    var el = document.createElement('fakeelement');
	    var transitions = {
	      'transition':'transitionEnd',
	      'OTransition':'oTransitionEnd',
	      'MSTransition':'msTransitionEnd',
	      'MozTransition':'transitionend',
	      'WebkitTransition':'webkitTransitionEnd'
	    }
	
	    for(t in transitions){
	        if( el.style[t] !== undefined ){
	            return t;
	        }
	    }
	}
	return whichTransitionEvent();
});
