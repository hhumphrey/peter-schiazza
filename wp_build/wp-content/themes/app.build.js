({
    appDir: "schiazza",
    baseUrl: "assets/scripts",
    dir: "schiazza_build",
    mainConfigFile: 'schiazza/assets/scripts/config.js',
	modules: [
        {
            name: "main"
        }
    ]
})