<?php

// Custom functions

function create_assignment_dropdown( ){
    $args = array( 
		'numberposts'       => -1,
        'post_type' => 'attachment', 
        'post_mime_type'=> 'image', 
        'order' => 'ASC', 
        'orderby' => 'title' );
    
    $posts = get_posts( $args );

    foreach( $posts as $post):
        $list[ $post->ID ] = $post->post_title;
    endforeach;
    
    return $list;
}

function get_images( $page_id ){
	return get_posts(
    	array(
        	'post_type'         => 'attachment',
	        'post_mime_type'    => 'image',
	        'numberposts'       => -1,
	        'post_parent'       => $page_id,
	        'orderby'           => 'menu_order',
	        'order'             => 'ASC'
        )
   	);
	
}

function get_images_from_gallery( $postContent ){
	$strStartSplit = '[gallery ids="';
	$strEndSplit = '"]"';
	$startSubstr = strrpos( $postContent, $strStartSplit  ) + strlen( $strStartSplit );
	$endSubstr = strrpos( $postContent, '"]' );
	$imageIds = explode( ',', substr( $postContent, $startSubstr, $endSubstr ) );
	
	$images = get_posts(
		array(
        	'post_type'         => 'attachment',
	        'post_mime_type'    => 'image',
			'post__in'			=> $imageIds,
	        'numberposts'       => -1,
	        'post_parent'       => $page_id,
			'orderby' => 'post__in'
        )
	);
	
	return $images;
}