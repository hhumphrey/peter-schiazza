<div id="js-thumbnails">
	<noscript>
	<?php while (have_posts()) : the_post(); 
		$parent_images = get_images( $post->ID );
		$all_images = create_image_output( $parent_images, $post  );
	
		$children = get_posts(
	    	array(
		        'numberposts'       => -1,
				'post_type'			=> 'page',
		        'post_parent'       => $post->ID,
		        'orderby'           => 'menu_order',
		        'order'             => 'ASC'
	        )
	   	);
	
		foreach( $all_images as $image ): 
		?>
		<div class="thumbnail">
			<a href="<?=$image["fullsize"]?>">
			    <figure>
			        <img src="<?=$image["thumbnail"]?>" />
			        <figcaption>
			            <p class="thumbnail-title"><?=$image["title"]?></p>
			            <p class="thumbnail-desc"><?=$image["description"]?></p>
			        </figcaption>
			    </figure>
			</a>
		</div>
		<? endforeach; ?>
	<?php endwhile; ?>
	</noscript>
</div>