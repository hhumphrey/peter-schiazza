<div id="js-thumbnails">
	<noscript>
	<?php while (have_posts()) : the_post(); 
		$images = get_images_from_gallery( $post->post_content );
		$all_images = create_image_output( $images, $post );
	
		foreach( $all_images as $image ): 
		?>
		<div class="thumbnail">
			<a href="<?=$image["fullsize"]?>">
			    <figure>
			        <img src="<?=$image["thumbnail"]?>" />
			        <figcaption>
			            <p class="thumbnail-title"><?=$image["title"]?></p>
			            <p class="thumbnail-desc"><?=$image["description"]?></p>
			        </figcaption>
			    </figure>
			</a>
		</div>
		<? endforeach; ?>
	<?php endwhile; ?>
	</noscript>
</div>