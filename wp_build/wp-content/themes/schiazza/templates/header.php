<div class="cnt-mast">
	<header class="hd-main">
		
		<div class="hd-main-shownav"></div>
		<h1><a class="hd-main-home" href="/"><img src="/assets/img/logo.png" alt="Peter Schiazza Photographer"></a></h1>
	</header>
	
	
	<div id="js-nav">
	<nav class="nav-main">
		<?php
	        if (has_nav_menu('primary_navigation')) :
	          wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => ''));
	        endif;
	      ?>
    </nav>

	<nav class="nav-secondary">
		<?php
	        if (has_nav_menu('secondary_navigation')) :
	          wp_nav_menu(array('theme_location' => 'secondary_navigation', 'menu_class' => ''));
	        endif;
	      ?>
    </nav>
	</div>
</div>