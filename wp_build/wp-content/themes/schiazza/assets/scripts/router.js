define([
  // Application.
  "app",
  
  "modules/navigation",
  "modules/images",
  "modules/content",	
  "modules/thumbnails",
  "modules/featuredimage",
	"matchmedia"
],

function( app, Navigation, Images, Content, Thumbnails, FeaturedImage ) {

  // Defining the application router, you can attach sub routers here.
  var Router = Backbone.Router.extend({
      initialize: function() {
          this.navigationCollection = new Navigation.Collection();
          this.imageCollection = new Images.Collection();
		  this.contentCollection = new Content.Collection();
          
          this.navListView = new Navigation.Views.List( { collection: this.navigationCollection } );
          this.imageListView = new Thumbnails.Views.List( { collection: this.imageCollection } );
          new FeaturedImage.Views.Item( { collection: this.imageCollection } );
		  new Content.Views.Page( { collection: this.contentCollection } );
    },
    routes: {
      "": "index",
	  "i:imageid": "preShowImage",
	  "category/:parentcat/i:imageid": "showImage_parentcat",
	  "category/:parentcat/:cat/i:imageid": "showImage_cat",
      "category/:parentcat/": "showCategory",
      "category/:parentcat/(:cat)/": "showCategory",
	  "content/:page/": "showPage"
    },
    index: function( imageId ) {
        this.reset( );
		this.imageCollection.category = 'home';
		this.imageCollection.action = 'get_aggregator_images';
		this.imageCollection.displayUrl = '/';
        this.imageCollection.fetch( );
		this.navigationCollection.reset( );
    },
	showPage: function( page ){
		this.reset( );
		this.contentCollection.page = page;
		this.contentCollection.fetch( );
		
		this.navListView.onnavclicked( page );
	},
    showCategory:function( parentCat, cat ){
		var that = this,
			_category =  ( cat ? cat : parentCat ),
			_action = ( cat ? 'get_images' : 'get_aggregator_images' ),
			displayUrl = '/category/' + parentCat + '/';
			
		this.reset( );
		displayUrl += cat ? cat + '/' : '';
        
        this.imageCollection.category = _category;
		this.imageCollection.action = _action;
		this.imageCollection.displayUrl = displayUrl;
        this.imageCollection.fetch( );
		
		this.navListView.onnavclicked( _category );
    },
	showImage: function( model, displayurl ){
		if ( !matchMedia('only screen and (max-width: 480px)').matches ) 
			this.imageListView.onshowimage( model, displayurl );
	},
    preShowImage:function( imageid, displayurl, category ){
		var model = this.imageCollection.get( imageid ),
			displayurl = ( (displayurl != null) ? displayurl : '/' ),
			that = this;
		
		if( category != null ){
			this.imageCollection.category = category;
			this.imageCollection.action = 'get_images';
			this.imageCollection.displayUrl = displayurl;
		}else{
			this.imageCollection.category = 'home';
			this.imageCollection.action = 'get_aggregator_images';
			this.imageCollection.displayUrl = '/';
		}
		
		this.imageCollection.displayImage = imageid;
		
		if ( !this.imageCollection.length ) {
			if( category )
				this.navListView.onnavclicked( category );
			
			this.imageCollection.fetch({
				success: function () {
					model = that.imageCollection.get( imageid );
					that.showImage( model, displayurl );
				}
			} );
		}else{
			this.showImage( model, displayurl, imageid, false );
		}
		
    },
	showImage_parentcat: function( parentCat, imageid ){
		this.preShowImage( imageid, '/category/' + parentCat + '/', parentCat);
		
	},
	showImage_cat: function( parentCat, cat, imageid ){
		this.preShowImage( imageid, '/category/' + parentCat + '/' + cat + '/', cat );
	},
    reset: function(){
        if ( this.imageCollection.length ) {
			this.imageCollection.displayImage = null;
            this.imageCollection.reset();
        }

		if ( this.contentCollection.length ) {
            this.contentCollection.reset();
        }
    }
  });

  return Router;

});
