//Filename: projection.js

define( [ 
        'jquery',
		'whichtransition' ], function( $ ){
    
	function getNavHeight( $item ){
		var cloned=$item.clone();
		cloned.addClass( 'is-hidden' ).attr('style','visibility:hidden;');	
		$item.parent().append(cloned);
		var height=cloned.find( 'ul' ).height();
		cloned.remove();

		return height;
	}		

    function initNav( ){
		$navMain = $( '.nav-main' );
		$navSecondary = $( '.nav-secondary' );
		$navMain.addClass( 'is-hidden' );
		$navSecondary.addClass( 'is-hidden' );
		$('.hd-main-shownav').on('click', function() { 			// toggle nav states
			$navMain.toggleClass( 'is-hidden' );
			$navSecondary.toggleClass( 'is-hidden' );
			
			/*
			if( $navMain.hasClass( 'is-hidden' ) ){
				
				if( $.support.transition ){
					$navMain.css( 'height', 0 );
					
					$navMain.one( $.support.transition.end, function( event ){
						setTimeout(function () { $navMain.removeAttr( 'style' ); }, 100);
						//$navMain.removeAttr( 'style' );
					});
					var finalHeight = getNavHeight( $navMain );
					$navMain.css( 'height', finalHeight );
				}
				
				$navMain.removeClass( 'is-hidden' );
				
			}else{
				
				$navMain.addClass( 'is-hidden' );
			}
			*/
		});
    }
    
    return {
        initNav: initNav
    }
    
});