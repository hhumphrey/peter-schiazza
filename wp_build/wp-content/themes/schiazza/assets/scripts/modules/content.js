define([
  // Application.
  "app",
	"matchmedia"
	
],

function( app ) {

    var Content = app.module();
  	
	Content.Views.Item = Backbone.View.extend({
		className: 'general-content',
		initialize: function(){
			this.$el.html( this.model.toJSON().content );
		},
		render: function(){
			if ( matchMedia('only screen and (min-width: 501px)').matches ){
				var fromTop = $( "#js-nav-" + this.model.get( 'title') ).offset().top - 25;
				this.$el.css( 'marginTop', fromTop );
			}
            return this;
        },
	});

	Content.Views.Page = Backbone.View.extend({
        el: '#js-thumbnails',
        initialize: function() {
			var that = this;
            this.listenTo( this.options.collection, {
                "reset": this.render
            });
        },
        render: function( ){
			this.$el.empty();
			if( this.options.collection.length > 0 ){
				var content = this.options.collection.models[0].get( 'content' );
				var item = new Content.Views.Item({
					model: this.options.collection.first()
				});
				//console.log( item.render().el );
				this.$el.append( item.render().el );
			}
        }
    });


    Content.Model = Backbone.Model.extend({
        defaults: {
        }
    });
  
    Content.Collection = Backbone.Collection.extend({
        model: Content.Model,
        url: function() {
            this.page =  ( this.page ? this.page : 'all' );
			var displayImage = this.displayImage ? "&selectedimage=" + this.displayImage : '';
            return "/wp/wp-admin/admin-ajax.php?action=get_page&page="+this.page;
        },
        parse: function( response ){
            return response.page;
        },
        initialize: function(models, options) {
          if (options){
            this.page = options.page;
			}
        },
        setSelected: function( model ){
            if( this.selectedModel )
				this.selectedModel.set( 'selected', false );
			
			this.selectedModel = model;
			this.selectedModel.set( 'selected', true );
        }
    });
  
    return Content;

});
