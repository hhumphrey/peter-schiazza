define([
  // Application.
  "app",
  "lodash",
  'text!templates/thumbnails/item.html',
  'text!templates/thumbnails/featureditem.html',
	'text!templates/thumbnails/itemloading.html',
	'preload',
	'whichtransition',
	'matchmedia',
	'hammer'
],

function( app, _, ItemTemplate, FeaturedItemTemplate, ItemLoadingTemplate ) {

    var Thumbnails = app.module();
  
    Thumbnails.Views.Item = Backbone.View.extend({
        template: ItemTemplate,
		className: 'thumbnail',
        events: {
            "click a": "onclick"
        },
        initialize: function(){
            this.listenTo( this.model, "change:selected", this.toggleSelected );  
        },
        render: function(){
			var objAtts = this.model.toJSON(),
				that = this,
				$figure = null;
			objAtts.currentUrl = this.options.displayUrl;
			
			if (matchMedia('only screen and (max-width: 480px)').matches) {
				objAtts.thumbnail = objAtts.mobile;
			}
			
            var sHtml = _.template( this.template, objAtts );
			
            this.$el.html( sHtml );
			
			$figure = $( this.$el.find( 'figure' ) );
			
			$.preload( [ objAtts.thumbnail ], {
				loaded_all: function( loaded, total ) {
					var randomTimeout = Math.floor( Math.random() * ( 500 - 100 + 1)) + 100;
					setTimeout( function () {
						$figure.removeClass( 'is-hidden' ); 
					}, randomTimeout );
				}
			});

            return this;
        },
		toggleSelected: function(){
			this.$el.toggleClass( 'selected' );
		},
        onclick: function( e ){
            this.trigger( 'itemclicked', this.model );
        }
    });

	Thumbnails.Views.FeaturedItem = Backbone.View.extend({
		id: "js-featureditem",
		className: 'featuredimage',
        template: FeaturedItemTemplate,
        initialize: function(){
			var that = this;
			$( window ).scroll( _.debounce( function(){
				that.onWindowScroll( );
			}, 300 ) );
        },
        render: function(){
            var objAtts = null,
				sHtml = _.template( this.template, objAtts );
            this.$el.html( sHtml );
            return this;
        },
		reset: function(){
			this.$el.remove();
		},
		onWindowScroll: function(){
			var $returnUp = $( '#js-fireturn--up' ),
				$returnDown = $( '#js-fireturn--down' );
				
			if(  $(window).scrollTop() > this.currentLocation ){
				$returnUp.removeClass( 'is-hidden' );
				$returnDown.addClass( 'is-hidden' );
			}else if( $(window).scrollTop() < this.currentLocation  ){
				$returnUp.addClass( 'is-hidden' );
				$returnDown.removeClass( 'is-hidden' );
			}else{
				$returnDown.addClass( 'is-hidden' );
				$returnUp.addClass( 'is-hidden' );
			}
		},
		urlOutput: function( url, text, strClass ){
			return '<a href="'+url+'" class="featureimage-control ' + strClass +'">' + text + '</a>';
		},
		showImage: function( ){
			var that = this,
				objAtts = this.model.toJSON();
				objAtts.hiddenClass = 'is-hidden';
				objAtts.prev = this.prevmodel ? this.urlOutput( this.displayurl + 'i' + this.prevmodel.get( 'id' ), 'Previous image', 'left' ) : null,
				objAtts.next = this.nextmodel ? this.urlOutput( this.displayurl + 'i' + this.nextmodel.get( 'id' ), 'Next image', 'right' ) : null,
				objAtts.displayurl = this.displayurl,
				sHtml = _.template( this.template, objAtts );
			
			// once image is preloaed show image
			$.preload( [ objAtts.fullsize ], {
				loaded_all: function( loaded, total ) {
					that.$el.html( sHtml );
					$figure = $( that.$el.find( 'figure' ) );
					$figcaption = $( that.$el.find( 'figcaption' ) );
					that.addReturnListeners( );
					that.addMobileSwipe( );
					
					var positionTop = $figure.height() + 25 - $figcaption.outerHeight();
					$figcaption.css( 'top', positionTop );
					
					setTimeout(function () { $figure.removeClass( 'is-hidden' ); }, 0);
				}
			});
		},
		addReturnListeners: function( ){
			var that = this;
			
			$( '.featuredimage-return' ).on( 'click', function( e ){
				e.preventDefault();
				that.focusOn();
			} )
		},
		addMobileSwipe:function(){
			var that = this;
			this.$el.find( '.featureimage-control' ).on( 'swiperight', function(){
				var href = that.$el.find( '.featureimage-control.right' ).attr( 'href' );
				Backbone.history.navigate( href, true);
			});
			this.$el.find( '.featureimage-control' ).on( 'swipeleft', function(){
				var href = that.$el.find( '.featureimage-control.left' ).attr( 'href' );
				Backbone.history.navigate( href, true);
			});
		},
		updateRender: function(  ){
			var that = this,
				$figure = $( this.$el.find( 'figure' ) );
			
			setTimeout(function () { $figure.addClass( 'is-hidden' ); }, 0);
			// hide featured item
			if( $.support.transition ){
				$figure.one( $.support.transition.end, function( event ){
					that.showImage( );
				});
			}else
				this.showImage( );
			
		},
		setvars: function( model, nextmodel, prevmodel, displayurl ){
			this.model = model;
			this.nextmodel = nextmodel;
			this.prevmodel = prevmodel;
			this.displayurl = displayurl;
		},
		focusOn: function(){
			this.currentLocation = this.$el.offset().top - 25;
			$('html, body').animate({ scrollTop: this.currentLocation }, 400 );
		}
    });
  
    Thumbnails.Views.List = Backbone.View.extend({
        el: '#js-thumbnails',
        initialize: function() {
			var that = this;
            this.listenTo( this.options.collection, {
                "reset": this.render
            });

            
			this.featuredItemView = new Thumbnails.Views.FeaturedItem({});
			this.featuredItemView.bind( 'navclicked', this.onnavclicked, this );
			this.itemLoadingTemplate = ItemLoadingTemplate;
			
			
        },
        render: function( ){
            var that = this;
			this.collectionCloned = this.options.collection.models.slice(0);
			this.extraScrollRows = 2
            this.$el.empty();

			
			//this.$el.addClass( 'is-loading' )
			
			this.fillScreen( 3 );
            
            $( window ).scroll( _.debounce( function(){
				that.onWindowScroll( );
			}, 300 ) );
			$( window ).resize( _.debounce( function(){
				that.fillScreen( 1 );
			}, 300 ) );
        },
		onWindowScroll: function( extraRows ){
			var pixelsFromWindowBottom = 0 + $(document).height() -  $(window).scrollTop() - $(window).height(),
				extraRows = 2;
			
			
			if( pixelsFromWindowBottom < 300 ){
				this.fillScreen( this.extraScrollRows );
				if (matchMedia('only screen and (max-width: 480px)').matches)
					this.extraScrollRows+=5;
				else
					this.extraScrollRows+=3;
				
				
			}
		},
		fillScreen: function( extraRows ){
			var _this = this,
				itemsHoriz = Math.floor( this.$el.width() / 163 ),
				itemsVert = ( Math.floor( $( window ).height() / 203 ) + extraRows ),
				itemsOnStage = this.$el.find( '.thumbnail' ).length,
				total = ( itemsHoriz * itemsVert );
				
				
			$( '.thumbnail-loading' ).remove( );
			
			if( total > itemsOnStage ){
				itemsToAdd = this.collectionCloned.splice( 0, ( total - itemsOnStage ) );
				
				_.each( itemsToAdd, function( thumbnail ){
					var thumb = new Thumbnails.Views.Item({
						model: thumbnail,
						id: 'thumbnail-' + thumbnail.id,
						displayUrl: this.options.collection.displayUrl
					});

					this.$el.append( thumb.render().el );

					thumb.bind( 'itemclicked', this.onitemclicked, this );
				}, this );
			}
			
			/* if( this.collectionCloned.length == 0 )
				this.$el.removeClass( 'is-loading' ); */
				
			if( this.collectionCloned.length != 0 )
				this.$el.append( this.itemLoadingTemplate );
			
				
		},
		renderFeaturedHolder: function( $thumbnail ){
			var $prevsibling = $thumbnail.prev(".thumbnail"),
				//$featureditem = $( '#js-featureditem' ),
				currentY = $thumbnail.position().top,
				siblingY = $prevsibling.length > 0 ? $prevsibling.position().top : 0,
				featuredTemplate = $( "#js-featureditem" ).length > 0 ? this.featuredItemView.el : this.featuredItemView.render().el;
				
			if( currentY != siblingY ){
				if( $thumbnail.prev().attr( 'id' ) != 'js-featureditem' ){
					this.featuredItemView.reset();
					$( featuredTemplate ).insertBefore( $thumbnail );
					this.featuredItemView.focusOn( );
				}
				
				this.featuredItemView.updateRender( );
			}else
				this.renderFeaturedHolder( $prevsibling );
		},
        onitemclicked: function( model ){
            this.options.collection.setSelected( model );
        },
		onshowimage: function( model, displayurl ){
			var selectedIndex = _.indexOf( this.options.collection.models, model ),
				nextIndex = (selectedIndex+1) == this.options.collection.length ? null : ( selectedIndex + 1),
				prevIndex = (selectedIndex-1) < 0 ? null : ( selectedIndex - 1),
				nextmodel = typeof( nextmodel ) != null ? this.options.collection.at( nextIndex ) : null,
				prevmodel = typeof( prevIndex ) != null ? this.options.collection.at( prevIndex ) : null;
			
			this.featuredItemView.setvars( model, nextmodel, prevmodel, displayurl );
			this.renderFeaturedHolder( $( "#thumbnail-" + model.get( 'id' ) ) );
			
			this.options.collection.setSelected( model );
			
		}
    });
  
    return Thumbnails;

});
