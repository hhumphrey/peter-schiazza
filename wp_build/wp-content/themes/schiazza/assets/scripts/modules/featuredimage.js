define([
  // Application.
  "app"
],

function( app ) {

  var FeaturedImage = app.module();
  
  FeaturedImage.Views.Item = Backbone.View.extend({
      events: {
          "click": "onclick"
      }, 
      initialize: function(){
          
      },
      onclick: function(){
          this.trigger( 'navclicked', this.model );
      }
  });
  
  return FeaturedImage;

});
