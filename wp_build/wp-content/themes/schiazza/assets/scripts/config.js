// Set the require.js configuration for your application.
require.config({

  // Initialize the application with the main application file.
  deps: ["main"],

  paths: {
    // JavaScript folders.
    libs: "../scripts/libs",
    plugins: "../scripts/plugins",
    templates: "../templates",

    // Libraries.
    jquery: "../scripts/libs/jquery",
    lodash: "../scripts/libs/lodash",
    backbone: "../scripts/libs/backbone",
    
    // Plugins
    text: "../scripts/plugins/text",

	// Utils
	animation: "../scripts/utils/animation",
	whichtransition: "../scripts/utils/whichtransition",
	preload: "../scripts/utils/preload",
	matchmedia: "../scripts/utils/matchmedia",
	hammer: "../scripts/utils/hammer",
	
	// Libs
	mobile: "../scripts/modules/mobile"
  },

  shim: {
    // Backbone library depends on lodash and jQuery.
    backbone: {
      deps: ["lodash", "jquery"],
      exports: "Backbone"
    },
	preload: ["jquery"],
	whichtransition: ["jquery"],
    // Backbone.LayoutManager depends on Backbone.
    "plugins/backbone.layoutmanager": ["backbone"]
  }

});
