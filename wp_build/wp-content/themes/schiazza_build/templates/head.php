<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Peter Schiazza</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
    
	<link rel="stylesheet" href="/assets/css/main.css">
	
	<!--[if (gt IE 6)&(lte IE 8)]>
	    <script type="text/javascript" src="/assets/scripts/libs/jquery.js"></script>
	    <script type="text/javascript" src="/assets/scripts/libs/css3-mediaqueries.js"></script>
	<![endif]-->
 
  <?php wp_head(); ?>
</head>
