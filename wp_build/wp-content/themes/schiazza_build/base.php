<?php get_template_part('templates/head'); ?>
<body>

	<!--[if lt IE 7]><div class="alert">Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</div><![endif]-->

	<? get_template_part('templates/header'); ?>

	<div role="main" class="cnt-main clearfix">
		<?php include roots_template_path(); ?>
	</div>

  <?php get_template_part('templates/footer'); ?>

</body>
</html>
