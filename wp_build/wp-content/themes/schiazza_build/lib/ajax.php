<?

add_action('wp_ajax_nopriv_get_page', 'ajax_get_page');
add_action('wp_ajax_get_page', 'ajax_get_page');

function ajax_get_page(){
	$args=array(
	  'name' => $_REQUEST['page'],
	  'post_type' => 'page',
	  'post_status' => 'publish',
	  'numberposts' => 1
	);
	$page = get_posts($args);
		
	$response = json_encode( array( 'success' => true, 'page' => array( 
		"content" => apply_filters('the_content', $page[0]->post_content ),
		"title" => $page[0]->post_name  )  ) );

	// response output
	header( "Content-Type: application/json" );
	echo $response;

	// IMPORTANT: don't forget to "exit"
	exit;
}

add_action( 'wp_ajax_get_aggregator_images', 'ajax_get_aggregator_images' );
add_action( 'wp_ajax_nopriv_get_aggregator_images', 'ajax_get_aggregator_images' );

function ajax_get_aggregator_images(){
	$selectedImage = get_post( $_REQUEST['selectedimage'] );
	
	$args=array(
	  'name' => $_REQUEST['page'],
	  'post_type' => 'page',
	  'post_status' => 'publish',
	  'numberposts' => 1
	);
	$parent_post = get_posts($args);
	
	$images = get_images_from_gallery( $parent_post[0]->post_content );
	
	$all_images = create_image_output( array( $selectedImage ), null, true, null );
	$all_images = array_merge( $all_images, create_image_output( $images, $parent_post[0], true, $selectedImage->ID  ) );
	
	$response = json_encode( array( 'success' => true, 'images' => $all_images  ) );

	// response output
	header( "Content-Type: application/json" );
	echo $response;

	// IMPORTANT: don't forget to "exit"
	exit;
}

add_action('wp_ajax_nopriv_get_images', 'ajax_get_images');
add_action('wp_ajax_get_images', 'ajax_get_images');
function ajax_get_images(){
	$selectedImage = get_post( $_REQUEST['selectedimage'] );
	
	$args=array(
	  'name' => $_REQUEST['page'],
	  'post_type' => 'page',
	  'post_status' => 'publish',
	  'numberposts' => 1
	);
	$parent_post = get_posts($args);
	
	$parent_images = get_images( $parent_post[0]->ID );
	$all_images = create_image_output( array( $selectedImage ), null, true, null );
	$all_images = array_merge( $all_images, create_image_output( $parent_images, $parent_post[0], true, $selectedImage->ID  ) );
	
	$children = get_posts(
    	array(
	        'numberposts'       => -1,
			'post_type'			=> 'page',
	        'post_parent'       => $parent_post[0]->ID,
	        'orderby'           => 'menu_order',
	        'order'             => 'ASC'
        )
   	);
	
	foreach( $children as $child ){
		$child_images = get_images( $child->ID );
		$all_images = array_merge( $all_images, create_image_output( $child_images, $child, false, $selectedImage->ID ) );
	}
	
     // now we'll write what the server should do with our request here
	$response = json_encode( array( 'success' => true, 'images' => $all_images  ) );

	// response output
	header( "Content-Type: application/json" );
	echo $response;

	// IMPORTANT: don't forget to "exit"
	exit;
}

function create_image_output( $arrImages, $parent, $booMulti = true, $selectedImage = null){
	$objOutput = array();
	
	foreach( $arrImages as $image ){
		if( $image->ID != intVal( $selectedImage ) ){
			$copy_parts = explode( '_', $image->post_title );
			$title = str_replace( '-', ' ', $copy_parts[0] );
			$description = substr( $copy_parts[1], 0, strrpos( $copy_parts[1], '-' ) );
			$description = str_replace( '-', ' ', $description );
		
			if( $booMulti )
				$category = sanitize_title( get_the_title( $image->post_parent ) );
			else
				$category = sanitize_title( $parent->post_title );
			
			$thumbnail = wp_get_attachment_image_src( $image->ID , 'thumbnail');
			$medium = wp_get_attachment_image_src( $image->ID , 'medium');
			$image_deets = array(
	        	'thumbnail'		=> $thumbnail[0],
		        'fullsize'		=> $image->guid,
		        'mobile'		=> $medium[0],
		        'category'		=> $category,
		        'title'			=> $title,
		        'description'	=> $description,
				'order'         => 'menu_order',
				'id' 			=> $image->ID
	        );
		
			array_push( $objOutput, $image_deets );
		}
	}
	
	return $objOutput;
}