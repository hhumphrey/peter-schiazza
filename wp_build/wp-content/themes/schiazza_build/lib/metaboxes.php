<?php

global $meta_boxes;

$meta_boxes = array();

/********************* GLOBAL META BOXES ***********************/
$allimages = create_assignment_dropdown( );

$meta_boxes[] = array(
    'id'    => 'featuredimages',
    'title' => 'Featured Images',
    'pages' => array( 'page' ),
    'fields' => array(
        array(
            'name' => 'Assign images',
            'id'   => "schiazza_assignedimages",
            'type' => 'select',
            'options' => $allimages,
            'std' => array( '0' ),
            'clone' => true
        ),
    ),
    'only_on'    => array(
        'template' => array( 'page-aggregator.php' )
    ),
);



/********************* META BOX REGISTERING ***********************/

/**
 * Register meta boxes
 *
 * @return void
 */
function KL_register_meta_boxes()
{
    global $meta_boxes;

    // Make sure there's no errors when the plugin is deactivated or during upgrade
    if ( class_exists( 'RW_Meta_Box' ) ) {
        foreach ( $meta_boxes as $meta_box ) {
            if ( isset( $meta_box['only_on'] ) && ! KL_maybe_include( $meta_box['only_on'] ) ) {
                continue;
            }

            new RW_Meta_Box( $meta_box );
        }
    }
}

add_action( 'admin_init', 'KL_register_meta_boxes' );

/**
 * Check if meta boxes is included
 *
 * @return bool
 */
function KL_maybe_include( $conditions ) {
    // Include in back-end only
    if ( ! defined( 'WP_ADMIN' ) || ! WP_ADMIN ) {
        return false;
    }

    // Always include for ajax
    if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
        return true;
    }

    if ( isset( $_GET['post'] ) ) {
        $post_id = $_GET['post'];
    }
    elseif ( isset( $_POST['post_ID'] ) ) {
        $post_id = $_POST['post_ID'];
    }
    else {
        $post_id = false;
    }

    $post_id = (int) $post_id;
    $post    = get_post( $post_id );

    foreach ( $conditions as $cond => $v ) {
        // Catch non-arrays too
        if ( ! is_array( $v ) ) {
            $v = array( $v );
        }

        switch ( $cond ) {
            case 'template':
                $template = get_post_meta( $post_id, '_wp_page_template', true );
                if ( in_array( $template, $v ) || $template == '' ) {
                    return true;
                }
            break;
        }
    }

    // If no condition matched
    return false;
}