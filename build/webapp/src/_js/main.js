require.config({
		"paths":{
			"jquery":"libs/jquery/jquery",
			"domReady":"plugins/domReady",
			"preload": "libs/preload",
			"mustache": "libs/mustache-wrap",
			"text": "plugins/text",
			"templates": "../sharedTemplates",
		},
		'shim': 
		{
			preload: {
				'deps': ['jquery']
			},
		}
});

define( [ 'plugins/domReady!' ],function( ) {
    /** 
    Root module
    @exports Main
    */
    PETERSCHIAZZA = {
        common: {
            init: function(  ) {
                //
                require( ['mylibs/ThumbnailLoad' ], function( ThumbnailLoad ){
                     ThumbnailLoad.init();
                });
            }
        },
        category: {
            init: function( ) {
                require( ['mylibs/CategoryGallery' ], function( CategoryGallery ){
                     CategoryGallery.init();
                });
            }
        }
    };
    
    UTIL = {
        exec: function( controller, action ) {
            var ns = PETERSCHIAZZA,
            action = ( action === undefined ) ? "init" : action;
            
            if ( controller !== "" && ns[controller] && typeof ns[controller][action] == "function" ) {
                ns[controller][action]();
            }
        },

        init: function() {
            var body = document.body,
            controller = body.getAttribute( "data-controller" ),
            action = body.getAttribute( "data-action" );
            
            UTIL.exec( "common" );
            UTIL.exec( controller );
            UTIL.exec( controller, action );
        }
    };
    
   UTIL.init();
    
   return { };
});
