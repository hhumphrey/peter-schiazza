define(["jquery","globals"
        ],
        function ($,globals) {
            return {
                RunTests: function () {
                	module("Globals");
                    test("restAPI set", function () {
                        equal(typeof globals.restAPI, "string",'Check Rest API string is  defined');
                        });
                    test("contentSelector exists", function () {
                    	console.log($(globals.contentSelector).length);
                        equal($(globals.contentSelector).length, 1,'ContentSelector element exists');
                        });                    
                    
                }
            };
});