define(["jquery",
         "globals",
          "app/viewManager",
          "views/homeView"
        ],
        function ($,globals,viewManager,HomeView) {
            return {
                RunTests: function () {
                module("View Manager");  
                var homeView=new HomeView();              
              
                
                    test("show View test", function () {
                    	 var elContent=$(globals.contentSelector);
                         var currentMarkup=elContent.html();
                         viewManager.showView(homeView);
                         var amendedMarkup=elContent.html();
                        notEqual(currentMarkup,amendedMarkup,'Check markup is amended');
                        });
                }
            };
});