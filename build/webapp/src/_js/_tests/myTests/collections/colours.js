define(["collections/colours"
        ],
        function (Collection) {
            return {
                RunTests: function () {
                	module("collections/colours");                    	
                	var myCollection= new Collection();
                	asyncTest('load remote data', function() {
                		myCollection.bind("reset", function(){
                				var count=myCollection.toJSON().length;
                			   ok((count>0),'Data available')
                			   start();
                		   }, this);
                		myCollection.fetch();
                		
                	});
                }
            };
});