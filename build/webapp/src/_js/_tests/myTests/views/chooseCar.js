define(["jquery","views/chooseCar"
        ],
        function ($,View) {
            return {
                RunTests: function () {
                	module("views/chooseCar");  
                	
                
                	test('render view', function() {
                		var myView= new View();
                		myView.render();
                		ok(($(myView.el).html().length>1),"check view is rendered");
                		
                	});
                	
                }
            };
});