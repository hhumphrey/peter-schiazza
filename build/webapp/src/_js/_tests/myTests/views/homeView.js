define(["jquery","views/homeView"
        ],
        function ($,View) {
            return {
                RunTests: function () {
                	module("views/homeView");                 	
                
                	test('render view', function() {
                		
                		var myView= new View();
                		myView.render();
                		ok(($(myView.el).html().length>1),"check view is rendered");
                		
                		
                	});
                	
                	
                }
            };
});