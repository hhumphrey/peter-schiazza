define(["jquery","views/notFound"
        ],
        function ($,View) {
            return {
                RunTests: function () {
                	module("views/notFound");                 	
                
                	test('render view', function() {
                		
                		var myView= new View();
                		myView.render();
                		ok(($(myView.el).html().length>1),"check view is rendered");
                		
                		
                	});
                	
                	
                }
            };
});