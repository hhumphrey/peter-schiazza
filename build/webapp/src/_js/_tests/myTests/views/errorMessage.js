define(["jquery","views/errorMessage"
        ],
        function ($,View) {
            return {
                RunTests: function () {
                	module("views/enterDetails.js");                 	
                
                	test('render view', function() {
                		expect(2)
                		var message="test";
                		var myView= new View({'message':message});
                		myView.render();
                		ok(($(myView.el).html().length>1),"check view is rendered");
                		console.log($(myView.el).html());
                		equal($(myView.el).find('.message').html(),message,"check error message is set");
                		
                	});
                	
                	
                }
            };
});