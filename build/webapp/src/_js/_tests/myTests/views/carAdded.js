define(["jquery","views/carAdded"
        ],
        function ($,View) {
            return {
                RunTests: function () {
                	module("views/carAdded");  
                	
                	var myView= new View();
                	test('render view', function() {
                		myView.render();
                		ok(($(myView.el).html().length>1),"check view is rendered");
                		
                	});
                }
            };
});