define(["jquery","views/carForm"
        ],
        function ($,View) {
            return {
                RunTests: function () {
                	module("views/carForm");                  	
                	
                	test('render view', function() {
                		var myView= new View();
                		myView.render();
                		ok(($(myView.el).html().length>1),"check view is rendered");
                		
                	});
                	test('Form elements', function() {
                		var myView= new View();
                		myView.render();
                		equal($(myView.el).find('select').length,3,"check nuber of form elements is 3");
                		
                	});
                }
            };
});