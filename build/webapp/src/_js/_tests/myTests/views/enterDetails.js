define(["jquery","views/enterDetails","models/car"
        ],
        function ($,View,Car) {
            return {
                RunTests: function () {
                	module("views/enterDetails");  
                	var myCar=new Car();
                	
                
                	test('render view', function() {
                		var myView= new View({'car':myCar});
                		myView.render();
                		ok(($(myView.el).html().length>1),"check view is rendered");
                		
                	});
                	test('check form elements', function() {
                		var myView= new View({'car':myCar});
                		myView.render();
                		equal($(myView.el).find("input").length,9,"There are 9 inputs");
                		
                	});
                	
                }
            };
});