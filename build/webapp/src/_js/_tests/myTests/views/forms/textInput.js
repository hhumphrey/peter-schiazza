define(["jquery","views/forms/textInput"
        ],
        function ($,View) {
            return {
                RunTests: function () {
                	module("views/selectBoxView");    
                	
                	var myView= new View({'name':'test'});
                	
                	
                	test('render view', function() {
                		myView.render();
                		ok(($(myView.el).html().length>1),"check view is rendered");
                		
                	});
                	
                	
                }
            };
});