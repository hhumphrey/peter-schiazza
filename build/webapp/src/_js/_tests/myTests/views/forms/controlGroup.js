define(["jquery","views/forms/controlGroup"
        ],
        function ($,View) {
            return {
                RunTests: function () {
                	module("views/controlGroup");  
                	var myView= new View({'name':'test','key':'test','formElement':$()});
                	test('render view', function() {
                		myView.render();
                		ok(($(myView.el).html().length>1),"check view is rendered");
                		
                	});
                }
            };
});