define(["jquery","views/forms/selectBox","collections/patterns"
        ],
        function ($,View,patterns) {
            return {
                RunTests: function () {
                	module("views/selectBoxView");    
                	var myCollection= new patterns();
                	var myView= new View({'model':myCollection});
                	
                	
                	asyncTest('render view', function() {
                		myCollection.bind("reset", function(){
                			expect(2);	
                			myView.render();
                			var count=myCollection.toJSON().length;
                			ok(($(myView.el).html().length>1),"check view is rendered");
                			equal($(myView.el).find('option').length,count,"correct number of options");	
                			   start();
                		   }, this);
                		myCollection.fetch();
                		
                	});
                	
                	
                }
            };
});