define(["models/car"
        ],
        function (Car) {
            return {
                RunTests: function () {
                	module("models/car");                       
                    test("validation::term accepted", function () { 
                    	var myCar=new Car();     
                    	expect( 2 );
                    	var testObj={terms:'n',name:'dd',email:'ff',age:18};
                    	 myCar.set(testObj);  
                    	 equal(myCar.isValid(),false,'Terms not accepted');
                    	 testObj.terms='y';
                    	 myCar.set(testObj);  
                    	 ok(myCar.isValid(),'Terms  accepted');
                        });                   
                    test("validation::name required", function () {  
                    	var myCar=new Car();     
                    	expect( 2 );
                    	var testObj={terms:'y',name:'',email:'ff',age:18};
                    	 myCar.set(testObj);  
                    	 equal(myCar.isValid(),false,'Name Empty');
                    	 testObj.name='dd';
                    	 myCar.set(testObj);  
                    	 ok(myCar.isValid(),'Name entered');
                       });
                    test("validation::email required", function () {   
                    	var myCar=new Car();     
                    	expect( 2 );
                    	var testObj={terms:'y',name:'dd',email:'',age:18};
                    	 myCar.set(testObj);  
                    	 equal(myCar.isValid(),false,'Email not entered');
                    	 testObj.email='dd';
                    	 myCar.set(testObj);  
                    	 ok(myCar.isValid(),'Email entered');
                       });
                    test("validation::age restriction over 18", function () {  
                    	var myCar=new Car();     
                    	expect( 2 );
                    	var testObj={terms:'y',name:'dd',email:'ff',age:17};
                    	 myCar.set(testObj);  
                    	 equal(myCar.isValid(),false,'under 18');
                    	 testObj.age=19;
                    	 myCar.set(testObj);  
                    	 ok(myCar.isValid(),'over 18');
                       });
                }
            };
});