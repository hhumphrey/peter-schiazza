//Filename: CategoryGallery.js

define( [ 
	'jquery', 
	'text!templates/gallery/gallery_highlightimage.html' ], function( $, templateHighlightImage ){

    var opts = {
        $parent : $( "#js-ps-thumbcontainer" )
    }
    
    var vars = {
        
    }

	function addFullsize( image, title, description ){
		/* var fullsizeImage = Mustache.to_html( templateHighlightImage, { 
			title : title,
			description : description,
			image : image
		});
		
		$( "#js-ps-thumbcontainer" ).parent().prepend( fullsizeImage ); */
	}
    
	function init( ){
       var arrImages = [];

		opts.$parent.on( 'click', '.js-ps-thumb', function( e ){ 
			e.preventDefault();
			
			var fullsizeImage = $( this ).attr( 'data-ps-fullsize' ),
				figtitle = $( this ).find( 'figcaption h1' ).html(),
				figsubtitle = $( this ).find( 'figcaption h2' ).html();
			
			addFullsize( fullsizeImage, figtitle, figsubtitle );
		})
       
    }
    
    return {
        init: init
    }
    
});