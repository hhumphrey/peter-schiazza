//Filename: Thumb.js

define([
	'jquery',
	'preload'
], function( $ ){
	
	var opts = {
        $parent : $( "#js-ps-thumbcontainer" ),
		thumbPrepend : "-160x200"
    }
    
    var vars = {
        
    }

	function preloadImages( arrImages ){
		$.preload( arrImages, {
	           loaded: function( image, loaded, total ) {
	               var imageUrl = $( image ).attr( 'src' ),
	                    $linkItem = $( $( 'a[data-ps-fullsize="' + imageUrl + '"]' ) ),
	                    randomTimeout = Math.floor(Math.random() * 400) + 100;

	               // add a random timeout

	               setTimeout(function(){
	                   $linkItem.parent().removeClass( 'is-hidden' );
	               }, randomTimeout );

	           },
	           loaded_all: function( loaded, total ) {

	           }
	       });
		
	}
    
    function init( ){
		var arrImages = [];

		opts.$parent.find( '.js-ps-thumb' ).each( function(){
			arrImages.push( $( this ).attr( 'data-ps-fullsize' ) );
		});
		
		preloadImages( arrImages );
	}
    
    return {
		init: init
	}
});