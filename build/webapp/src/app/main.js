require([
	"plugins/domReady!",
	// Application.
	"app",
	// Main Router.
	"router",
	"mobile",
	"enquirejs",
	"modernizr",
	"bootstrap"
],

function( DomReady,app, Router, Mobile ) {
	function loadApp(){
		
		app.router = new Router( window.photoData );
		Backbone.history.start({ pushState: true, root: app.root });
		
		$(document).on("click", "a[href]:not([data-bypass])", function(evt) {

		    // Get the absolute anchor href.
		    var href = { prop: $(this).prop("href"), attr: $(this).attr("href") };
		    // Get the absolute root.
		    var root = location.protocol + "//" + location.host + app.root;

		    // Ensure the root is part of the anchor href, meaning it's relative.
		    if (href.prop.slice(0, root.length) === root) {
		      // Stop the default event to ensure the link will not cause a page
		      // refresh.
		      evt.preventDefault();

		      // `Backbone.history.navigate` is sufficient for all Routers and will
		      // trigger the correct events. The Router's internal `navigate` method
		      // calls this anyways.  The fragment is sliced from the root.
		      Backbone.history.navigate(href.attr, true);
		    }
		  });
	}
	
	
	$(document).ready(function() {
		
		if( !Modernizr.touch )
			loadApp();
		else{
			enquire.register("screen and (min-device-width : 320px) and (max-device-width : 480px)", {
				match : function() {
					new Mobile( );
				},
				unmatch: function() {
					loadApp();
				}
			}).fire();
		}
	  
	});

});
