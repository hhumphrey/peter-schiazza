define([
  	// Application.
  	"app",
	'jquery',
	'lodash',
	'backbone',
	'views/imagelist',
	'views/scroller',
	'views/navlist'
],

function( app, $, _, Backbone, ImageList, ImageScroller, NavList ) {

  // Defining the application router, you can attach sub routers here.
  var Router = Backbone.Router.extend({
	isFirstRoute:true,
	thumbCountHoriz: 0,
	thumbCountVert: 0,
	currentCat : '',
    routes: {
		"": "index",
		"category/:cat/": "category",
		"category/:cat/show/:id": "featuredimage",
		"category/:primaryslug/:secondaryslug/": "subcat",
		"category/:primaryslug/:secondaryslug/show/:id": "subcat_featuredimage",
		"page/:slug" : "page"
    },
	initialize: function( photoData ){
		var _this = this;
		this.resetThumbCounts( );
		
		var thumbsOnStage = this.thumbCountHoriz * this.thumbCountVert ;
		
		this.imageList = new ImageList( { 
			photoData: photoData,
			onstage: thumbsOnStage
		});
		this.imageList.bind( 'thumbselected', function( model ){
			_this.updateScroller( model ); 
		} )
		
		this.imageScroller = ImageScroller;
		this.imageScroller.bind( 'navclicked', function( modelId ){
			var model = _this.imageList.collection.get( modelId );
			
			_this.imageList.collection.setSelected( model );
			_this.updateScroller( _this.imageList.collection.get( modelId ) );
		} );
		
		this.primaryNav = new NavList({
			el: $("#js-ps-primarynav")
		});
		
		$(window).resize( _.debounce( function(){
			_this.onWindowResize( _this );
		}, 300 ) );
		
		$( window ).scroll( _.debounce( function(){
			_this.onUserScroll( _this );
		}, 300 ) );
		
	},
	index: function() {
		var items = this.imageList.getAllThumbs( );
		
		this.checkResetPhotos( );
		
		if( $( '.content').find( this.imageScroller.el ).length > 0 )
			this.imageScroller.removeScroller( );
		
		
		
		this.filterThumbs( items );
		this.currentCat = '';
		this.isFirstRoute = false;
	},
	checkResetPhotos: function(){
		if( $( '#js-ps-page' ).length > 0 ){
			$( '#js-ps-page' ).remove();
		}
	},
	category: function( cat ){
		var items = this.getAllFilteredItems( cat );
		this.checkResetPhotos( );
		this.filterThumbs( items );
		
		if( $( '.content').find( this.imageScroller.el ).length > 0 )
			this.imageScroller.removeScroller( );
			
		this.currentCat = cat;
		this.isFirstRoute = false;
	},
	featuredimage: function( cat, id ){
		var _this = this,
			selectedmodel = this.imageList.collection.get( id );
		this.checkResetPhotos( );	
		if( this.isFirstRoute )
			this.updateScroller( selectedmodel, cat );
		
		this.currentCat = cat;
		this.isFirstRoute = false;
	},
	subcat: function( parent, cat ){
		this.category( cat );
	},
	subcat_featuredimage: function( cat, subcat, id ){
		this.featuredimage( subcat, id );
	},
	page: function( slug ){
		var _this = this;
		
		if( $( '.content').find( this.imageScroller.el ).length > 0 )
			this.imageScroller.removeScroller( );
			
		this.imageList.bind( 'thumbshidden', function(){
			_this.imageList.unbind( 'thumbshidden' );
			
		} );

		this.imageList.hideThumbs( );
	},
	updateScroller: function( selectedmodel, cat ){
		var _this = this,
			imagescrollerCount = $( '.content').find( this.imageScroller.el ).length,
			booSwapImmediate = false;
		
		if( imagescrollerCount == 0 ) // a new load
			this.imageList.unshiftModel( selectedmodel );
		
		var filteredImages = this.getAllFilteredItems( selectedmodel.get( 'parent' ) );
			modelIndex = _.indexOf( filteredImages, selectedmodel ),
			prevModel = filteredImages[ modelIndex - 1 ],
			nextModel = filteredImages[ modelIndex + 1 ];
		
		// Do I need to reset the scroller position
		if( imagescrollerCount != 0 ){
			// get distance from selected to imagescroller
			var $thumb = $( "#image-" + selectedmodel.get( 'id' ) ),
				$scroller = $( $( '.content').find( this.imageScroller.el ) ),
				selectedFromTop = $thumb.position().top,
				distanceFromScrollerUnder = selectedFromTop - ( $scroller.position().top + $scroller.height() ),
				distanceFromScrollerOver = $scroller.position().top - ( selectedFromTop + 160 );
			
			if( distanceFromScrollerUnder > 160 || distanceFromScrollerOver > 0 ){
				// get last line before current line
				booSwapImmediate = true;
				
				if( selectedFromTop > 50 ){
					for( var i=modelIndex; i > 0; i-- ){
						var thumbModel = filteredImages[i],
							$thumb = $( "#image-" + thumbModel.get( 'id' ) );
					
						if( $thumb.position().top != selectedFromTop ){
							// insert scroller just below
						
							$scroller.detach().insertAfter( $thumb );
							// break
							i = 0;
						}
					}
				}else{ // top row eventuality
					
					var thumbModel = filteredImages[0],
						$thumb = $( "#image-" + thumbModel.get( 'id' ) );
					
					$scroller.detach().insertBefore( $thumb );
					
				}
				
			}
		}
		
		this.imageScroller.setModel( selectedmodel, prevModel, nextModel );
		
		if( imagescrollerCount == 0 )
			this.filterThumbs( filteredImages, true );
		else
			this.imageScroller.swapImage( booSwapImmediate );
		
		
	},
	getAllFilteredItems: function( cat ){
		if( cat == '' ){
			var items = this.imageList.getAllThumbs( );
		}else{
			var items = this.imageList.getFilteredThumbs( cat ),
			    _this = this;
		
			_.each( this.primaryNav.navWithParent( cat ), function( item, i ){
			    var subcat = item.get( 'slug' ).replace( cat, '' ).replace(/\//g,'');
			
			    items = items.concat( _this.imageList.getFilteredThumbs( subcat ) );
			} );
		}
		
		return items;
	},
	onWindowResize: function( _this ){
		var currentCount = _this.thumbCountHoriz * _this.thumbCountVert,
			newHoriz = Math.round( $( '.content' ).width() / 163 ),
			newVert = Math.round( $( window ).height() / 163 ),
			newCount = newHoriz * newVert;
			
		if( newCount > currentCount ){
			var items = _this.getAllFilteredItems( _this.currentCat );
			_this.imageList.filterThumbs( items, newCount, currentCount );
			
			_this.thumbCountHoriz = newHoriz;
			_this.thumbCountVert = newVert;
			
		}
		
		//adjust height of the scroller
		this.imageScroller.adjustHeight();
		
	},
	onUserScroll: function( _this ){
		var pixelsFromWindowBottom = 0 + $(document).height() -  $(window).scrollTop() - $(window).height();
		
		if( pixelsFromWindowBottom < 300 ){
			var currentCount = _this.thumbCountHoriz * _this.thumbCountVert
				newCount = currentCount + _this.thumbCountHoriz;
			var filteredThumbs = _this.getAllFilteredItems( _this.currentCat ); 
			
			_this.imageList.filterThumbs( filteredThumbs, newCount, currentCount );
			_this.thumbCountVert++;
		}
			
			
	},
	nearbottom: function() {
		var opts = this.options,
					pixelsFromWindowBottomToBottom = 0 + $(document).height() - (opts.binder.scrollTop()) - $(window).height();

 		// if behavior is defined and this function is extended, call that instead of default
		if (!!opts.behavior && this['_nearbottom_'+opts.behavior] !== undefined) {
			return this['_nearbottom_'+opts.behavior].call(this);
		}

		this._debug('math:', pixelsFromWindowBottomToBottom, opts.pixelsFromNavToBottom);

	            // if distance remaining in the scroll (including buffer) is less than the orignal nav to bottom....
		return (pixelsFromWindowBottomToBottom - opts.bufferPx < opts.pixelsFromNavToBottom);

	},
	filterThumbs: function( items, booShowScroller ){
		var _this = this;
		
		this.resetThumbCounts( );
		
		this.imageList.bind( 'thumbshidden', function(){
			_this.imageList.unbind( 'thumbshidden' );
			_this.imageList.filterThumbs( items, _this.thumbCountHoriz * _this.thumbCountVert, 0 );
			
			if( booShowScroller ){
				$( '.content' ).prepend( _this.imageScroller.renderit( ).el );
			}
		} );
		
		this.imageList.bind( 'thumbsfiltered', function(){
			_this.imageList.unbind( 'thumbsfiltered' );
			_this.imageList.showThumbs( );
			
			if( booShowScroller ){
				_this.imageScroller.showScroller();
			}
			
		} );
		
		this.imageList.hideThumbs( );
	},
	resetThumbCounts: function(){
		this.thumbCountHoriz = Math.floor( $( '.content' ).width() / 163 );
		this.thumbCountVert = Math.floor( $( window ).height() / 163 );
	}
  });

  return Router;

});
