// filename: app/models/nav.js

define(['lodash', 'backbone'], function(_, Backbone) {
	var ImageModel = Backbone.Model.extend({
		// Default attributes for the todo.
		defaults: {
			slug: '',
			parent: null,
			selected: false
		},
		initialize: function(){
			
		}
	});
	return ImageModel;
});
