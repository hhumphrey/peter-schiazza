// filename: app/models/image.js

define(['lodash', 'backbone'], function(_, Backbone) {
	var ImageModel = Backbone.Model.extend({
		// Default attributes for the todo.
		defaults: {
			fullsize: '',
			large: '',
			thumbnail: '',
			parent: '',
			title: '',
			content: '',
			slug: '',
			visibility: false,
			selected: false
		},
		initialize: function(){
			/*if( $( '#image-'+this.get( 'id' ) ).length > 0 )
				this.set( 'visibility', true );*/
			
			//console.log( this.get( 'id' ) );
		}
	});
	return ImageModel;
});
