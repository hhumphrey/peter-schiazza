// Set the require.js configuration for your application.
require.config({

  // Initialize the application with the main application file.
  deps: ["main"],

  paths: {
    // JavaScript folders.
    libs: "../js/libs",
    plugins: "../js/plugins",
    vendor: "../vendor",
	
    // Libraries.
    jquery: "../js/libs/jquery",
    lodash: "../js/libs/lodash",
    backbone: "../js/libs/backbone",
	preload: "../js/libs/preload",
	matchmedia: "../js/libs/matchmedia",
	enquirejs: "../js/libs/enquire",
	modernizr: "../js/libs/modernizr.custom",
	mobile: "../js/mylibs/mobile",
	bootstrap: "../js/libs/bootstrap",
	
	// Plugins.
	text: "../js/plugins/text",
	
	// Other.
	templates: "../sharedTemplates"
  },

  shim: {
    // Backbone library depends on lodash and jQuery.
    backbone: {
      deps: ["lodash", "jquery"],
      exports: "Backbone"
    },
	preload: {
      deps: [ "jquery"]
    },
	enquirejs: {
      deps: [ "matchmedia"]
    },
    // Backbone.LayoutManager depends on Backbone.
    "plugins/backbone.layoutmanager": ["backbone"],
	"utils/transitions": ["jquery"],
	"libs/scrollto": ["jquery"],
	"bootstrap": ["jquery"]
	
  }

});
