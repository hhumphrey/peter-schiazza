<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * dataModel
 *
 * @package		Legacy
 *
 */
class datamodel extends CI_Model {

	function __construct()
	{
		parent::__construct();
		 $this->load->library('curl');
         $this->apiUrl=$this->config->item('api-url');
	}

	/**
	 * get array of colours
	 * @return array
	 */
	function getRecentPosts()
	{
	  $url=$this->apiUrl.'/get_recent_posts';
	  $strResponse=$this->curl->simple_get($url);
	  return json_decode($strResponse);
	}
	
    /**
    */
    function getNav( $menu_slug )
    {
        $url = $this->apiUrl.'/esf/get_menu/?dev=1&menu_slug='.$menu_slug;
        $strResponse = $this->curl->simple_get( $url );
        return json_decode( $strResponse );
    }
    
    function getPagedImages( $paged = 1 )
    {
        $url = $this->apiUrl.'/esf/get_paged_images/?dev=1&paged='.$paged;
        $strResponse = $this->curl->simple_get( $url );
        return json_decode( $strResponse );
    }
    
    function getAllImages( )
    {
        $url = $this->apiUrl.'/esf/get_all_images/?dev=1';
        $strResponse = $this->curl->simple_get( $url );
        return json_decode( $strResponse );
    }
    
    /**
    */
    function getImageCount( )
    {
        $url = $this->apiUrl.'/esf/get_image_count';
        $strResponse = $this->curl->simple_get( $url );
        return json_decode( $strResponse );
    }

	function getChildren( $parentId )
    {
        $url = $this->apiUrl.'/esf/get_page_children/?dev=1&parentid='.$parentId;
        $strResponse = $this->curl->simple_get( $url );
        return json_decode( $strResponse );
    }
	/**
	 */
	function getPages()
	{
	    return$this->getPage(24);
	}

/**
	 */
	function getHome()
	{
	  return$this->getPage(112);
	}

	/**
	 */
	function getPanels()
	{
	  return$this->getPage(29);
	}

/**
	 */
	function getPage($id)
	{
	   $url=$this->apiUrl.'/get_page/?id='.$id.'&children=y';
	  $strResponse=$this->curl->simple_get($url);
	  return json_decode($strResponse);
	}

    function getPageFromSlug( $slug )
    {
        $url=$this->apiUrl.'/get_page/?slug='.$slug;
        $strResponse=$this->curl->simple_get($url);
        return json_decode($strResponse);
    }




}
