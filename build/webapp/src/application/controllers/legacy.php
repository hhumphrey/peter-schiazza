<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Controller for legacy site
 * @author helenhumphrey
 *
 * @package		Legacy
 *
 */
class Legacy extends CI_Controller {

    public function __construct()
       {
           parent::__construct();
           // Your own constructor code

           $this->load->library('templateparser');
		   $this->load->library('pagination');
           $this->load->model('Datamodel');
           $this->load->helper('url');
           $this->jsonData=array();
           $this->arrClass=array('first','second','third','forth','fifth');
           $this->version='1';
           $this->apiUrl=$this->config->item('api-url');
           $this->adminUrl = $this->config->item('admin-url');
			$this->siteUrl = $this->config->item('CDN');
           
           $this->imagesPerPage = 30;
       }

    /**
    * Displays home view
    */
    public function index()
    {
        $this->title="Wordpress + Codeigniter";
        //$allImages = $this->Datamodel->getPagedImages( $page_number )->images;
        
        $data = array(
            'nav'=>$this->getNav(),
            'main' => $this->getPagedImages( )
        );
        
        $container=$this->templateparser->parseTemplate('layout/container.html', $data );
        $this->displayContent( $container, 'home' );
    }

	/*public function page()
	{
		$slug =  $this->uri->segment(2);
		$data = $this->Datamodel->getPageFromSlug( $slug );
		
		$pageContent = $this->templateparser->parseTemplate( 'page.html', $data->page );
		
		$data = array(
            'nav'=>$this->getNav(),
            'main' => $pageContent
        );
		
		$container=$this->templateparser->parseTemplate('layout/container.html', $data );
		$this->displayContent( $container, 'page', false );
	}*/
    
    public function category()
    {
        $this->title="Wordpress + Codeigniter";
       	// GET URI SEGMENTS
        //$pagenumber = isset( $this->uri->uri_string( 'page' ) ) ? $this->uri->uri_string( 'page' ) : 0 ;
        $pagevariables['show'] = isset( $urisegments['show'] ) ? $urisegments['show'] : null;

		$segments = $this->uri->segment_array();
		$booCategoryFound = false;
		$booPageFound = false;
		$booShowFound = false;
		$category = '';
		$pagevariables = array( "show"=>null, "page"=>null, "pagesegment"=>null );
		$count = 1;
		foreach($segments as $segment) {
			if( $booCategoryFound &&  $segment != 'show' &&  $segment != 'page' )
				$category .= '/'.$segment;
			
			if( $booShowFound )
				$pagevariables['show'] = $segment;
			if( $booPageFound ){
				$pagevariables['page'] = $segment;
				$pagevariables['pagesegment'] = $count;
			}
			
		    if ( $segment == "category" )
				$booCategoryFound = true;
			else if( $segment == 'page' ){
				$booCategoryFound = false;
				$booPageFound = true;
			}
			else if( $segment == 'show' ){
				$booCategoryFound = false;
				$booPageFound = false;
				$booShowFound = true;
			}
			$count++;
		}
		$parentPage = $this->Datamodel->getPageFromSlug( $category );
		$childrenPages = $this->Datamodel->getChildren( $parentPage->page->id );
		
		
		
		foreach( $childrenPages->children as $child ){
			$data = $this->Datamodel->getPageFromSlug( $parentPage->page->slug.'/'.$child->post_name );
			$parentPage->page->attachments = array_merge( $parentPage->page->attachments, $data->page->attachments );
		}
		
        $imagearray = array();
        $highlighthtml = '';

		// HIGHLIGHT IMAGE
        if( $pagevariables['show'] ){
	
            /*** NEED TO RESTRUCTURE ARRAY TO BRING IMAGE TO START ***/
            foreach( $parentPage->page->attachments as $image ){
                if( $image->id == $pagevariables['show'] ){
                    $image->thumbclass = 'is-highlight';
                    array_unshift( $imagearray, $image );

                    // only show the highlight image on the first page
                    if( $pagevariables['page'] == null ){
                        $data = array(
                            'title' => $image->title,
                            'description' => $image->description,
                            'fullsize' => $this->adminUrl.$image->images->full->url,
							'nextlink' => '/category'.$parentPage->page->url.'show/'.$parentPage->page->attachments[1]->id
                        );
                        
						$highlighthtml = '<div class="featured-image is-hidden" id="js-ps-featuredimage">';
                        $highlighthtml .= $this->templateparser->parseTemplate('gallery/gallery_highlightimage.html', $data );
                    	$highlighthtml .= '</div>';
					}
                }
                else{
                    $image->thumbclass = '';
                    array_push( $imagearray, $image );
                }
            }
        }else
            $imagearray = $parentPage->page->attachments;

        /**** PAGINATION ****/
        $urlappend = !$pagevariables['show'] ? $category : $category.'/'.$pagevariables['show'];

        $config['base_url'] = $this->siteUrl.'/category/'.$category.'/page/';
        $config['total_rows'] = count( $parentPage->page->attachments );
        $config['per_page'] = $this->imagesPerPage;
        $config['uri_segment'] = $pagevariables['pagesegment'];
        $this->pagination->initialize($config);

        $data = array(
            'imageSource' => $this->adminUrl,
            'parentslug' => $category,
            'highlightImage' => $highlighthtml,
            'images' => array_slice( $imagearray, $pagevariables['page'], $this->imagesPerPage ),
            'pagination' => $this->pagination->create_links(),
			'pageSlug' => $parentPage->page->url
        );
        
        $categoryImages = $this->templateparser->parseTemplate( 'gallery/gallery--category.html', $data );
        
        $data = array(
            'nav'=>$this->getNav(),
            'main' => $categoryImages
        );
        
        $container=$this->templateparser->parseTemplate('layout/container.html', $data );
        
        $this->displayContent( $container, 'category' );
    }
    
    /**
    *
     */

/*
<li class="parent" data-slug="/portrait/" class="">
        <a href="/category/portrait/">Portrait</a>
        <ul>
            <li data-slug="/portrait/portrait-sub/" class=""><a href="/category/portrait/portrait-sub/">Portrait Sub 1</a></li>
            <li data-slug="/portrait/portrait-sub/" class=""><a href="/category/portrait/portrait-sub-2/">Portrait Sub 2</a></li>
        </ul>
    </li>
    <li class="parent" data-slug="/landscape/" class=""><a href="/category/landscape/">Landscape</a></li>


<li data-slug={{url}}  class="{{class}}"><a href="/category{{url}}">{{title}}</a></li>
*/
     public function getNav($slug="",$state='closed'){
		$primary_nav_data = $this->Datamodel->getNav( 'primary_navigation' )->nav_items;
		
		$menu_array = array();
		
		foreach( $primary_nav_data as $nav ){
			//$primary_nav .= '<li data-slug="'.$nav->url.'"  class=""><a href="/category'.$nav->url.'">'.$nav->title.'</a></li>';
			//if( $nav->)
			
			if( $nav->post_parent == 0 ){
				array_push( $menu_array, $nav );
				$current_parent = $menu_array[ count( $menu_array ) - 1 ];
				$current_parent->children = array();
			}
			else
				array_push( $current_parent->children, $nav );
		}
		
		$primary_nav = '';
		foreach( $menu_array as $nav ){
			$primary_nav .= '<li class="parent" data-slug="'.$nav->url.'"><a href="/category'.$nav->url.'">'.$nav->title.'</a><button class="caret" data-toggle="collapse" data-parent="#js-ps-primarynav" href="#collapse'.$nav->ID.'" class="accordion-toggle">Expand</button>';
			if( $nav->children ){
				$primary_nav .= '<ul id="collapse'.$nav->ID.'" class="accordion-body collapse">';
				foreach( $nav->children as $child ){
					$primary_nav .= '<li data-slug="'.$child->url.'"><a href="/category'.$child->url.'">'.$child->title.'</a>';
				}
				$primary_nav .= '</ul>';
			}
			$primary_nav .= '</li>';
		
		}
		
		$data = array(
            'primary_nav_html' => $primary_nav,
			'secondary_nav' => $this->Datamodel->getNav( 'secondary_navigation' )->nav_items
        );
        
        return  $this->templateparser->parseTemplate( 'nav.html', $data );
    }
    /**
    *
    */
    public function getPagedImages( ){
        $page_number = $this->uri->segment( 2, 0 ) / $this->imagesPerPage + 1;
        $images = $this->Datamodel->getPagedImages( $page_number )->images;
        $config['base_url'] = $this->siteUrl.'/page/';
        $config['total_rows'] = $this->Datamodel->getImageCount()->imagecount;
        $config['per_page'] = $this->imagesPerPage;
        $config['uri_segment'] = 2;

        $this->pagination->initialize($config); 
        $data = array(
            'images' => $images,
            'pagination' => $this->pagination->create_links()
        );
        
        return  $this->templateparser->parseTemplate( 'gallery/gallery.html', $data );
    }
	/**
	 *Display specified content
	 * @param $strContent
	 * @param $isQunit
	 */
	private function displayContent( $container, $bodyClass, $booDisplayJS = true ){
		$scriptData = array(
        	  //  'title'=>$this->title,
        		'script'=>'app/main_v'.$this->version,
        		'version'=>$this->version,
        	 	'jsonData'=>$this->jsonData,
        		'CDNPath'=>getCDN(),
        	);

		$footerScript = '';
		
		if( $booDisplayJS ){
		    $allImages = $this->Datamodel->getAllImages( )->images;
		    $jsonData = json_encode( $allImages );
	    
		    $this->load->helper('cdn');
	        $scriptData=array(
	        	  //  'title'=>$this->title,
	        		'script'=>'app/main_v'.$this->version,
	        		'version'=>$this->version,
	        	 	'jsonData'=>$this->jsonData,
	        		'CDNPath'=>getCDN(),
	        		'jsonData' => $jsonData
	        	);
			$footerScript=$this->templateparser->parseTemplate('layout/footerScript.html',$scriptData,true);
		}
		
	     //$this->jsonData['homeData']=$this->Datamodel->getHome();
    	$headScript=$this->templateparser->parseTemplate('layout/headScript.html',$scriptData,true);
    	

	     echo $this->templateparser->parseTemplate('layout/layout.html',array(
    	     	'container'=>$container,
    	        'headScript'=>$headScript,
	     		'footerScript'=>$footerScript,
	     		'bodyClass' => $bodyClass
	         )
	     );
	}
}