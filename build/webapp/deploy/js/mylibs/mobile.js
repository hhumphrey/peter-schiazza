//Filename: Carousel.js

define([
	'jquery',
	'lodash', 
	'text!templates/gallery/overlay-modal.html',
], function( $, _, template_modal ){
	
	function Mobile( ){
		
		this.init = function( ){
			$('body').addClass( 'mobile' );
			
			$( '.photo-list a' ).on( 'click', function( e ){
				e.preventDefault();
				
				
				var title = $( this ).parent().find( 'h1' ).html(),
					content = $( this ).parent().find( 'h2' ).html()
					fullsize = $( this ).data( 'ps-fullsize' );
					
				var modal = _.template( template_modal,  { title: title, content: content, fullsize: fullsize } );
				
				$( 'body' ).prepend( modal );
				$( '.gallery-model_close' ).on( 'click', function( e ){
					e.preventDefault();
					
					
					$( '.gallery-modal' ).remove();
				})
			} )
		}
		
		this.init();
	}
	
	return Mobile;
});

/*


*/