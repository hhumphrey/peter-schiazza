// filename: scripts/views/category.js

define([
	'jquery', 
	'lodash', 
	'backbone',
	'text!templates/gallery/thumbnail.html',
	'preload',
	'utils/transitions'
], function($, _, Backbone, categoryTemplate){
	_.templateSettings = {
		interpolate : /\{\{([\s\S]+?)\}\}/g
	};
	var ImageView = Backbone.View.extend({
		tagName: 'li',
		template: categoryTemplate,
		events: {
			'click a' : "imageClicked"
		},
		initialize: function(){
			var json = this.model.toJSON();
			json.hiddenclass = 'is-hidden';
			
			if( $( "#image-" + this.model.get( 'id' ) ).length > 0 )
				this.$el = $( "#image-" + this.model.get( 'id' ) );
			else{
				$( this.el ).html( _.template( this.template, json ) );
			}
			
			this.model.bind('change:visibility', this.toggleVisibility, this);
			this.model.bind('change:selected', this.toggleSelected, this);
		},
		toggleVisibility: function(){
			var _this = this;
			
			if( this.model.get( 'visibility' ) ){ // SHOW IMAGE
				this.showThumb( );
			}else{ // HIDE IMAGE
				var $figure = this.$el.find( 'figure' );
				$figure.toggleClass('is-hidden');
				this.$el.detach();
				this.trigger( 'thumbremoved', this );
			}
		},
		toggleSelected: function(){
			
			if( this.model.get( 'selected' ) )
				$( this.el ).addClass( 'is-highlight' );
			else
				$( this.el ).removeClass( 'is-highlight' );
		},
		showThumb: function(){
			var _this = this;
			
			if( $( '#image-'+this.model.get( 'id' ) ).length == 0 ){
				this.trigger( 'addthumb', this, 'image-'+this.model.get( 'id' ) );
			}
			
			
			$.preload( [ _this.model.get( 'thumbnail' ) ], {
				loaded_all: function( loaded, total ) {
					var randomTimeout = Math.floor(Math.random() * 200) + 50;
					setTimeout(function(){
						_this.$el.find( 'figure' ).removeClass( 'is-hidden' );
					}, randomTimeout );
				}
			});
		},
		imageClicked: function( e ){
			e.preventDefault();
			
			//Backbone.history.navigate( '/category/' + this.model.get( 'parent' ) + '/' + this.model.get( 'id' ), true);
			this.trigger( 'thumbclicked', this.model );
		}
    });
    return ImageView;
});
