// Filename: views/imagelist
define([
	'jquery', 
	'lodash', 
	'backbone',
	// Pull in the Collection module from above
	"collections/navitems"

], function($, _, Backbone, NavCollection ){
	var imageListView = Backbone.View.extend({
		onstage: 0,
		currentCat: 'showall',
		initialize: function( ){
			var _this = this,
				arrItems = [];
				
			arrItems = this.loopLevel( $( this.el ), '.parent', null, arrItems );
			
			this.collection = new NavCollection( arrItems );
		},
		loopLevel: function( $el, selector, parentSlug, arrItems ){
			var _this = this;
			
			$el.find( selector ).each( function(){
				
				var obj = {
					slug: $( this ).data( 'slug' ),
					parent: parentSlug != null ? parentSlug.replace(/\//g,'') : null
				}
				
				arrItems.push( obj );
				
				$( this ).find( 'ul' ).each( function(){
					_this.loopLevel( $( this ), 'li', obj.slug, arrItems  )
				});
			})
			
			return arrItems;
			
		},
		createThumb: function( model ){
			
		},
		navWithParent: function( strParent ){
			return this.collection.where({ parent: strParent });
		}
	});
	return imageListView;
});
