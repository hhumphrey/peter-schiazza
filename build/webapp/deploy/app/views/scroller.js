// filename: scripts/views/category.js

define([
	'jquery', 
	'lodash', 
	'backbone',
	'text!templates/gallery/gallery_highlightimage.html',
	'preload',
	'libs/whichtransition',
	'utils/transitions',
	'preload'
], function($, _, Backbone, highlightTemplate ){
	_.templateSettings = {
		interpolate : /\{\{([\s\S]+?)\}\}/g
	};
	
	var ImageScroller = Backbone.View.extend({
		tagName: 'div',
		className: 'featured-image is-hidden',
		template: _.template( highlightTemplate ),
		attributes: {
			id: 'js-ps-featuredimage'
		},
		events: {
			'click .featured-image_nav-item a' : "navClicked",
			'click .featured-image_nav-overlay' : "navClicked"
		},
		initialize: function(){
			$( "#" + this.attributes.id ).remove();
		},
		renderit: function( ){
			var _this = this;
			var json = this.model.toJSON( );
			
			json.prevlink = this.getUrlFromModel( this.prevModel ),
			json.prevId = this.getIdFromModel( this.prevModel ),
			json.nextlink = this.getUrlFromModel( this.nextModel ),
			json.nextId = this.getIdFromModel( this.nextModel );
			
			$( this.el ).html( this.template( json ) );
			
			return this;
		},
		getIdFromModel: function( model ){
			if( model )
				return model.get( 'id' );
			else
				return 0;
		},
		getUrlFromModel: function( model ){
			if( model )
				return '/category'+model.get( 'slug' )+'show/'+model.get( 'id' );
			else
				return '/category'+this.model.get( 'slug' )+'show/'+this.model.get( 'id' );
		},
		showScroller: function( ){
			var _this = this;
			
			$('html, body').animate({ scrollTop: $("#js-ps-featuredimage").offset().top - 25 }, 600 );
			
			$.preload( [ this.model.get( 'fullsize' ) ], {
				loaded_all: function( loaded, total ) {
					_this.adjustHeight();
					
					$( _this.el ).removeClass( 'is-hidden' );
					
				}
			});
			
		},
		setModel: function( model, prevModel, nextModel ){
			this.model = model;
			this.prevModel = prevModel;
			this.nextModel = nextModel;
		},
		swapImage: function( booImmediate ){
			var _this = this;
			
			this.adjustHeight();
			
			if( booImmediate ){
				$( this.el ).addClass( 'is-hidden' );
				this.afterSwapImage();
			}else{
				$( _this.el ).addClass( 'is-hidden' );
				
				if( $.support.transition ){
					$( this.el ).one( $.support.transition.end, function( event ){
						_this.afterSwapImage();
					});
				}else
					_this.afterSwapImage();
				
				
			}
			
		},
		adjustHeight: function(){
			var height = $( this.el ).find( 'img' ).height();
			$( this.el ).find( '.featured-image_inner' ).height( height );
			$( this.el ).height( height );
		},
		afterSwapImage: function( ){
			this.renderit();
			this.showScroller();
			//$( this.el ).css( 'height', 'auto' );
		},
		removeScroller: function( ){
			var _this = this;
			
			if( $.support.transition ){
				$( this.el ).find( 'img' ).one( $.support.transition.end, function( event ){
					if (event.target !== event.currentTarget) return;
					$( _this.el ).detach();
				});
			}else
				$( _this.el ).detach();
			
			$( this.el ).addClass( 'is-hidden' );
		},
		navClicked: function( event ){
			event.preventDefault();
			var modelId = $(event.target).data('modelid');
			
			if( modelId != 0 )
				this.trigger( 'navclicked', $(event.target).data('modelid') );
		}
    });
    return new ImageScroller;
});
