// Filename: views/imagelist
define([
	'jquery', 
	'lodash', 
	'backbone',
	// Pull in the Collection module from above
	"collections/images",
	"views/image",
	'utils/transitions'
], function($, _, Backbone, ImageCollection, ImageView){
	var imageListView = Backbone.View.extend({
		el: $("#js-ps-thumbcontainer ul"),
		onstage: 0,
		currentCat: 'showall',
		initialize: function( options){
			$.extend(this,options);
			
			this.collection = ImageCollection;
			
			this.collection.bind( 'reset', this.resetImages, this );
			//this.collection.bind( 'thumbselected', this.reorder, this );
			
			this.collection = ImageCollection.reset( this.options.photoData );
		},
		render: function( ){
			
			return this;
		},
		createThumb: function( model ){
			var view = new ImageView({
					model: model
				});
			
			view.bind( 'addthumb', this.addThumb, this );
			view.bind( 'thumbremoved', this.thumbremoved, this );
			view.bind( 'thumbclicked', this.thumbclicked, this );
		},
		thumbremoved: function( ){
			this.onstage--;
			
			if( this.onstage == 0 ){
				this.trigger( 'thumbshidden' );
			}
		},
		thumbclicked: function( model ){
			this.collection.setSelected( model );
			
			this.trigger( 'thumbselected', this.collection.selectedModel )
		},
		reorder: function( model ){
			if( model )
				$( '#image-'+model.get( 'id' ) ).prependTo( this.el );
		},
		resetImages: function( ){
			$( this.el ).empty();
			this.collection.each( this.createThumb, this );
		},
		addThumb: function( view, id  ){
			this.$el.append( view.$el.attr( 'id', id ) );
		},
		hideThumbs: function( booShowScroller ){
			var _this = this;
			
			var visibleThumbs = this.collection.where({ visibility: true });
			
			if( $.support.transition ){
				$( this.el ).one( $.support.transition.end, function( event ){
					for( var i=0; i<visibleThumbs.length; i++ ){
						visibleThumbs[i].set( 'visibility', false );
					}
					
					$( _this.el ).removeClass( 'is-hidden' );
					
					_this.trigger( 'thumbshidden' );
				});
			}else{
				for( var i=0; i<visibleThumbs.length; i++ ){
					visibleThumbs[i].set( 'visibility', false );
				}
				
				$( _this.el ).removeClass( 'is-hidden' );
				
				_this.trigger( 'thumbshidden' );
			}
			
			$( this.el ).addClass( 'is-hidden' );
			
			
		},
		showThumbs: function( ){
			this.$el.removeClass( 'is-hidden' );
		},
		changeThumbsOnStage: function( onstage ){
			this.onstage = onstage;
		},
		filterThumbs: function( items, onstage, start ){
			var _this = this;
			
			this.onstage = onstage > items.length ? items.length : onstage;
			
			if( start <= items.length || items == '' ){
				for( var i = start; i<this.onstage; i++ ){
					items[i].set( 'visibility', true );
				}
			}
			
			setTimeout(function() {
				_this.trigger( 'thumbsfiltered' );
			}, 100 );
		},
		getFilteredThumbs: function( filterCat ){
			var items = this.collection.models;
			
			//this.currentCat = filterCat == '' ? this.currentCat : filterCat;
			
			//if( this.currentCat != 'showall' )
			items = this.collection.where({ parent: filterCat });
			
			return items;
		},
		getAllThumbs: function(){
			return this.collection.models;
		},
		resetOrder: function( ){
			this.collection.sort( );
		},
		unshiftModel: function( model ){
			var modelId = model.get( 'id' );
			
			this.collection.remove([ model ]).unshift( model );
			//this.collection
			
			
		}
	});
	return imageListView;
});
