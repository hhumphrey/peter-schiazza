// filename: app/collection/images.js

define([
	'lodash', 
	'backbone',
	'models/image'
  ], function( _, Backbone, ImageModel ){
	var ImageCollection = Backbone.Collection.extend({
		// Reference to this collection's model.
		model: ImageModel,
		parse: function ( response ) {
			return response.page.attachments;
		},
		setSelected: function( model ){
			if( this.selectedModel )
				this.selectedModel.set( 'selected', false );
				
			this.selectedModel = model;
			this.selectedModel.set( 'selected', true );
		},
		getSelected: function( ){
			var selected = this.selectedModel;
			this.selectedModel = null;
			
			return selected;
		},
		haveSelected: function(){
			if( this.selectedModel )
				return true;
			else
				return false;
		}
     });
	
	return new ImageCollection;
});
