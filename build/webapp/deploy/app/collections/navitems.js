// filename: app/collection/images.js

define([
	'lodash', 
	'backbone',
	'models/nav'
  ], function( _, Backbone, NavModel ){
	var NavCollection = Backbone.Collection.extend({
		// Reference to this collection's model.
		model: NavModel,
		setSelected: function( model ){
			if( this.selectedModel )
				this.selectedModel.set( 'selected', false );
				
			this.selectedModel = model;
			this.selectedModel.set( 'selected', true );
		},
		getSelected: function( ){
			var selected = this.selectedModel;
			this.selectedModel = null;
			
			return selected;
		}
     });
	
	return NavCollection;
});
