<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
*/


class ServiceProxy extends CI_Controller
{

  public function __construct()
       {
          parent::__construct();
          $this->load->library('proxy');
          $this->load->model('Datamodel');
         $this->API=getenv('APIURL');
       }

       public function _remap($object_called, $arguments)
       {

		$this->request = new stdClass();
		$this->request->method = $this->_detect_method();
		$controller_method = $object_called.'_'.	$this->request->method;
		call_user_func_array(array($this, $controller_method), $arguments);

       }

/**
	 * Detect method
	 *
	 * Detect which method (POST, PUT, GET, DELETE) is being used
	 *
	 * @return string
	 */
	protected function _detect_method()
	{
		$method = strtolower($this->input->server('REQUEST_METHOD'));

		if ($this->config->item('enable_emulate_request'))
		{
			if ($this->input->post('_method'))
			{
				$method = strtolower($this->input->post('_method'));
			}
	        	else if ($this->input->server('HTTP_X_HTTP_METHOD_OVERRIDE'))
		        {
		            $method = strtolower($this->input->server('HTTP_X_HTTP_METHOD_OVERRIDE'));
		        }
		}

		if (in_array($method, array('get', 'delete', 'post', 'put')))
		{
			return $method;
		}

		return 'get';
	}



     /**
     * Get getRecentPosts
     */
    public function recentPosts_get()
    {
      echo json_encode($this->Datamodel->getRecentPosts());
    }

   /**
     * Get getRecentPosts
     */
    public function pages_get()
    {
      echo json_encode($this->Datamodel->getPages());
    }



}