({
	 mainConfigFile: '../src/app/config.js',
	 paths: {
			"globals": "empty:",			
	 },
	 use: { 
		   "modernizr": {
			     attach: "Modernizr"
			   }			
	 },
	appDir: "../src",
	baseUrl:"./app",
	fileExclusionRegExp: /^(\.|\_)/,
	dir: "../deploy",   
	 modules: [	        
		           {
		     	  name:'app/main'
	
		           }		          
	
	],  
	optimizeCss : "standard",
	preserveLicenseComments: false

})

