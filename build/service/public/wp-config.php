<?php

/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
/** The name of the database for WordPress */
define('DB_NAME', getenv('MYSQL_DB_NAME'));

/** MySQL database username */
define('DB_USER', getenv('MYSQL_USERNAME'));

/** MySQL database password */
define('DB_PASSWORD', getenv('MYSQL_PASSWORD'));

/** MySQL hostname */
define('DB_HOST', getenv('MYSQL_DB_HOST'));

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'KP|nwl>LEY5)3>Qh*=1{BjUp.Orw-k)9n&kdimb4!x],il6Fxw6v?Phn*q(bA^TS');
define('SECURE_AUTH_KEY',  'K6Z7>xsb*/[-`DiPkL4}[r+;Ef@DHbV@#Jj+-W-b6rR]/QUSpRH0ay_`_N/+vLkC');
define('LOGGED_IN_KEY',    'Nn`*}?8K(>5haqay2_6dHPl||x-5,}NOK@t>BB1yrTkuN~p?Vy6}XX(&-MZ~alA*');
define('NONCE_KEY',        '3pN<dB[0)cQx|w^8ka,8_c$Xi5wgN2+|Gr*o5Q@-jRu/vPq@v*iQIjfa78E[X+^*');
define('AUTH_SALT',        '>Dh$B>abVL`JywOqYT0h3 q|xR5u=yt=`bZ84]MLkE+WD/U+K AohB#8@~-fe.w!');
define('SECURE_AUTH_SALT', 'gS0|tEo_ucY;EkcA8`@j5/ruG7dP>Kqvrb]8wGm><<rW3Z-%qotp)KrJsL}R;4>I');
define('LOGGED_IN_SALT',   'R5uvW6FbVJ!l|8~n[UQS-lwqhYbS?Q(K&hXF=^Ej?fmyz;%ZIxq@BA,(nS-17XQ_');
define('NONCE_SALT',       'xi|<cVroUQ^Lz:X .7<L[m`8Yeg+:[F,TKo`6^&F.KN,mvr2d#F&=2_kzLOKx!U-');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

//define('WP_SITEURL', getenv('WP_SITEURL'));
//define('WP_HOME', getenv('WP_SITEURL'));


define('WP_HOME', 'http://' . $_SERVER['HTTP_HOST']);
define('WP_SITEURL', 'http://' . $_SERVER['HTTP_HOST']);
define('WP_CONTENT_URL', '/wp-content');
define('DOMAIN_CURRENT_SITE', $_SERVER['HTTP_HOST']);



/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
