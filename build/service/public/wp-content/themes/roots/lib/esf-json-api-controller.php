<?php

class JSON_API_ESF_Controller {
  public function get_menu() {
      global $json_api;
      
      if (!$json_api->query->menu_slug  ) {
          $json_api->error("Include a 'menu_slug' in the query var.");
      }
      
      if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[ $json_api->query->menu_slug ] ) ) {
          $menu = wp_get_nav_menu_object( $locations[ $json_api->query->menu_slug ] );
          
          $menu_items = wp_get_nav_menu_items($menu->term_id);
      }
      
    return array(
      "nav_items" => $menu_items
    );
  }
  
  public function get_paged_images( ){
      global $json_api;
      
      $paged = !$json_api->query->paged ? 1 : $json_api->query->paged;
      $posts_per_page = !$json_api->query->posts_per_page ? 30 : $json_api->query->posts_per_page;
      
      $query_images_args = array(
          'post_type' => 'attachment', 
          'post_mime_type' =>'image', 
          'post_status' => 'inherit', 
          'posts_per_page' => $posts_per_page, 
          'paged' => $paged
      );

      $query_images = new WP_Query( $query_images_args );

      return array(
             "images" => $this->get_image_data( $query_images->posts )
             );
  }
  
  public function get_all_images( ){
       global $json_api;

       $query_images_args = array(
           'post_type' => 'attachment', 
           'post_mime_type' =>'image', 
           'post_status' => 'inherit',
			'posts_per_page' => -1
       );

       $query_images = new WP_Query( $query_images_args );
       
              
       return array(
           "images" => $this->get_image_data( $query_images->posts )
           );
   }
   
   public function get_image_data( $posts ){
       $images = array();
       
       foreach ( $posts  as $image) {
              $thumbnail = wp_get_attachment_image_src( $image->ID, 'thumbnail' );
              $fullsize = wp_get_attachment_image_src( $image->ID, 'large' );
				
				$slug = get_permalink( $image->post_parent );
              $parent = str_replace( " ", "-", strtolower( get_the_title( $image->post_parent ) ) );

              // array( "landscape" => 'blue', 1 => 'red', 2 => 'green', 3 => 'red');
              $image_object = array(
                  'id' => $image->ID,
                  'thumbnail' => site_url().$thumbnail[0],
                  'fullsize' => site_url().$fullsize[0],
                  'title' => $image->post_title,
                  'content' => $image->post_content,
                  'parent' => str_replace( " ", "-", $parent ),
					'slug' => $slug
              );

              array_push( $images, $image_object );
          }
          return $images;
          
          
   }
  
  public function get_image_count( ){
      $query_images_args = array(
            'post_type' => 'attachment', 
            'post_mime_type' =>'image', 
            'post_status' => 'inherit',
            'numberposts' => -1
        );

        $query_images = get_posts( $query_images_args );
        
        return array(
            "imagecount" => count( $query_images )
        );
  }

	public function get_navigation( ){
		$menu_name = 'categories';
		if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[ $menu_name ] ) ) {
			$menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
			$menu_items = wp_get_nav_menu_items($menu->term_id);
		}
		
		return array(
			"menu" => $menu_items
		);
	}
	
	public function get_page_children( ){
		global $json_api;

	      $parentid = !$json_api->query->parentid ? 1 : $json_api->query->parentid;
		
		$query_pages_args = array(
			'post_type' => 'page', 
			'numberposts' => -1,
			'post_parent'     => $parentid,
		);
		
		$pages = get_posts( $query_pages_args );
		
		return array(
			"children" => $pages
		);
	}

}

?>