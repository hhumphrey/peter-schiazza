define([
  // Application.
  "app"
],

function( app ) {

    var Images = app.module();
  
    Images.Model = Backbone.Model.extend({
        defaults: {
        }
    });
  
    Images.Collection = Backbone.Collection.extend({
        model: Images.Model,
        url: function() {
            this.category =  ( this.category ? this.category : 'all' );
            return "http://schiazza.jstemplates/data/"+this.category+".json";
        },
        parse: function( response ){
            return response.images;
        },
        initialize: function(models, options) {
          if (options)
            this.category = options.collection;
        },
        setSelected: function( model ){
            if( this.selectedModel )
				this.selectedModel.set( 'selected', false );
			
			this.selectedModel = model;
			this.selectedModel.set( 'selected', true );
        }
    });
  
    return Images;

});
