define([
  // Application.
  "app",
	"animation"
],

function( app, AnimationUtils ) {

  var Navigation = app.module();
  
  Navigation.Model = Backbone.Model.extend({
      // Default attributes for the todo.
      defaults: {
        category: '',
        parent: '',
        selected: false,
		softselected: false
      },
      // Toggle the `selected` state
      toggle: function() {
          var toggleVal = !this.get("selected");
          this.set( {"selected": !this.get("selected") } );
          
          
      },
      // Ensure some content has been filled in
      validate: function( attrs ) {
        
      }
  });
  
  Navigation.Collection = Backbone.Collection.extend({
      model: Navigation.Model,
      initialize: function(){
          //console.log( 'Navigation.Collection :: initialize' );
      },
      getParentModel: function( model ){
          var parentModel = this.get( 'js-nav-' + model.get( 'parent' ) );
          return parentModel;
      },
      isSibling: function( oldModel, newModel ){
           var oldParent = this.getParentModel( oldModel ),
               newParent = this.getParentModel( newModel );
               
            if( oldParent && newParent ){
                if( oldParent.get( 'id' ) == newParent.get( 'id' ) )
                    return true;   
            }
            
            return false;
           
      },
	  setSoftSeleted: function( model ){
		 	if( this.softSelectedModel )
				this.softSelectedModel.set( 'softselected', false );
			this.softSelectedModel = model;
			console.log( this.softSelectedModel );
			this.softSelectedModel.set( 'softselected', true );
	  },
      setSelected: function( model, strValue ){
          // get parent          
          var parentModel = this.getParentModel( model );
          if( parentModel )
            parentModel.set( strValue, true );
          
          model.set( strValue, true );
          
          if( this.selectedModel && this.selectedModel.get( 'id' ) != model.get( 'id' ) ){
              if( this.isSibling( this.selectedModel, model ) ){ // is sibling in sub level
                  this.selectedModel.set( strValue, false );
              }else{
                  if( parentModel ){ // is sub level
                      if( this.selectedModel.get( 'id' ) != parentModel.get( 'id' ) ) // parent wasn't last selected
                          this.selectedModel.set( strValue, false );
                  }else{ // is top level
                      this.selectedModel.set( strValue, false );
                      var oldParent = this.getParentModel( this.selectedModel );
                      
                      if( oldParent && oldParent.get( 'id' ) != model.get( 'id' ) )
                          oldParent.set( strValue, false );
                  }
              }
          }
          
          this.selectedModel = model;
      },
      setSelectedById: function( strId ){
          var model = this.get( strId );
      },
      getSelectedModel: function( ){
          return this.selectedModel;
      },
	  reset: function( ){
		if( this.selectedModel ){
			var parentModel = this.getParentModel( this.selectedModel );
			
			this.selectedModel.set( 'selected', false );
			if( parentModel )
	            parentModel.set( 'selected', false );
	
			this.selectedModel = null;
		}
		//this.setSelected( null );
	  }
  });
  
  Navigation.Views.Item = Backbone.View.extend({
      events: {
          "click": "onclick"
      }, 
      initialize: function(){
          this.model.bind('change:selected', this.toggleSelected, this);
		  this.model.bind('change:softselected', this.toggleSoftSelected, this);
      },
      onclick: function(){
          this.trigger( 'navclicked', this.model );
      },
	  getFinalHeight: function( $item ){
		var cloned=$item.clone();
		cloned.addClass( 'selected' ).attr('style','visibility:hidden;');	
		$item.parent().append(cloned);
		var height=cloned.find( 'ul' ).height();
		cloned.remove();

		return height;
	  },
	  toggleSoftSelected: function(){
	  	console.log( 'toggleSoftSelected' );
	
		this.$el.parent().toggleClass( 'softselected' );
	  },
      toggleSelected: function(){
		var $target = this.$el.parent(),
			$dropdown = $target.find( 'ul' );
		
		if( !$target.hasClass( 'selected' ) ){
			if( $dropdown.length > 0 ){
				var finalHeight = this.getFinalHeight( $target );
				$dropdown.css( 'height', finalHeight );
			}
			
			$target.addClass( 'selected' );
		}else{
			if( $dropdown.length > 0 )
				$dropdown.removeAttr( 'style' );
			$target.removeClass( 'selected' );
		}
		//AnimationUtils.getFinalHeight( $dropdown )
          //this.$el.parent().toggleClass( 'selected' );
      }
  });
  
  Navigation.Views.List = Backbone.View.extend({
      el: '#js-nav',
      initialize: function(){
          var _this = this;
          this.collection = this.options.collection;
          
          this.$el.find( 'li a' ).each( function(){
              var _category = $( this ).data( 'category' ),
                _parent = $( this ).data( 'parent' ),
                _id = 'js-nav-' + _category;
                            
             _this.collection.add( [ { selected: false, category: _category, parent: _parent, id: _id } ] );
          });
          
          this.collection.each( this.setupNavItems, this );          
      },
      setupNavItems: function( model ){
           var view = new Navigation.Views.Item({
              model: model,
              el: '#js-nav-' + model.get( 'category' )
          });
            
          view.bind( 'navclicked', this.onnavclicked, this );
      },
      onnavclicked: function( model ){
			this.$el.find( '.softselected' ).removeClass( 'softselected' );
          this.collection.setSelected( model, 'selected' );
          this.trigger( 'navselected', this.collection.selectedModel );
      },
	  onshowimage: function( _category ){
		var model = this.collection.where( { category: _category } );
		this.collection.setSelected( model[0], 'softselected' );
	  }
  });
  
  return Navigation;

});
