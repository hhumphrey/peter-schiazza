define([
  // Application.
  "app",
  
  "modules/navigation",
  "modules/images",
  "modules/thumbnails",
  "modules/featuredimage"
],

function( app, Navigation, Images, Thumbnails, FeaturedImage ) {

  // Defining the application router, you can attach sub routers here.
  var Router = Backbone.Router.extend({
      initialize: function() {
          this.navigationCollection = new Navigation.Collection();
          this.imageCollection = new Images.Collection();
          
          this.navListView = new Navigation.Views.List( { collection: this.navigationCollection } );
          this.imageListView = new Thumbnails.Views.List( { collection: this.imageCollection } );
          new FeaturedImage.Views.Item( { collection: this.imageCollection } );
    },
    routes: {
      "": "index",
	  "i:imageid": "showImage",
	  "category/:parentcat/i:imageid": "showImage_parentcat",
	  "category/:parentcat/:cat/i:imageid": "showImage_cat",
      "category/:parentcat": "showCategory",
      "category/:parentcat/(:cat)": "showCategory"
    },
    index: function( imageId ) {
        this.reset( );
		this.imageCollection.category = 'all';
        this.imageCollection.fetch( );
		this.navigationCollection.reset( );
    },
    showCategory:function( parentCat, cat ){
        this.reset( );
        
        var _category =  ( cat ? cat : parentCat );
        
        this.imageCollection.category = _category;
        this.imageCollection.fetch( );
        
        var selectedNav = this.navigationCollection.where({ category: _category });
        
    },
    showImage:function( imageid, displayurl ){
		var model = this.imageCollection.get( imageid ),
			displayurl = ( (displayurl != null) ? displayurl : '/' );
		
		if( !this.imageCollection.selectedModel ){
			//
			var model = this.imageCollection.where( { id: imageid } );
			this.imageCollection.setSelected( model );
		}
		
		this.imageListView.onshowimage( model, displayurl );
		this.navListView.onshowimage( model.get( 'category' ) );
    },
	showImage_parentcat: function( parentCat, imageid ){
		if ( !this.imageCollection.length ) {
			this.imageCollection.category = parentCat;
	        this.imageCollection.fetch( );
		}
		
		this.showImage( imageid, '/category/' + parentCat + '/' );
	},
	showImage_cat: function( parentCat, cat, imageid ){
		if ( !this.imageCollection.length ) {
			this.imageCollection.category = cat;
	        this.imageCollection.fetch( );
		}
		
		this.showImage( imageid, '/category/' + parentCat + '/' + cat + '/' );
	},
    reset: function(){
        if ( this.imageCollection.length ) {
            this.imageCollection.reset();
        }
    }
  });

  return Router;

});
