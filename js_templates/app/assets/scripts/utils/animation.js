define([], function() {	
	function getFinalHeight( $item ){
		var cloned=$item.clone();
		cloned.removeClass( 'is-hidden' ).attr('style','visibility:hidden;');	
		$item.parent().append(cloned);
		var height=cloned.height();
		cloned.remove();
		
		return height;			
	}
	
	return{
		getFinalHeight: getFinalHeight
	}
});
